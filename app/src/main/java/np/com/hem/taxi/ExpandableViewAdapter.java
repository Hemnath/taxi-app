package np.com.hem.taxi;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by manish on 6/6/2016.
 */
public class ExpandableViewAdapter extends BaseExpandableListAdapter {

    private final DBHelper mydb;
    private Context mContext;
    private HashMap<GroupItem, List<ChildItem>> childItems = new HashMap<GroupItem, List<ChildItem>>();
    private HashMap<GroupItem, List<ChildItem>> childSearch = new HashMap<GroupItem, List<ChildItem>>();
    private List<GroupItem> groupItems = new ArrayList<GroupItem>();
    private List<GroupItem> groupSearch = new ArrayList<GroupItem>();
    private LayoutInflater inflater;

    public ExpandableViewAdapter(Context context) {
        this.mContext = context;
        mydb = new DBHelper(context);
    }

    public void setOrderList(Context context) {
        ArrayList<Integer> array_list;
        ArrayList<Integer> array_list_child;
        array_list = mydb.getAllDistricts();
        int count = array_list.size();
        groupItems.clear();
        groupSearch.clear();
        childItems.clear();
        childSearch.clear();
        for (int i = 0; i < count; i++) {
            Cursor rs = mydb.getDistricts(array_list.get(i));
            rs.moveToFirst();
            int gId = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_ID));
            int districtsId = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_DistrictsId));
            String districtsName = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_DistrictsName));
            GroupItem groupItem = new GroupItem(gId, districtsId, districtsName);

            array_list_child = mydb.getAllPlace(gId);

            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            int count1 = array_list_child.size();
            for (int j = 0; j < count1; j++) {
                Cursor palceRs = mydb.getPlace(array_list_child.get(j));
                palceRs.moveToFirst();
                int cId = palceRs.getInt(palceRs.getColumnIndex(DBHelper.COLUMN_ID));
                int palceId = palceRs.getInt(palceRs.getColumnIndex(DBHelper.COLUMN_PalceId));
                String placeName1 = palceRs.getString(palceRs.getColumnIndex(DBHelper.COLUMN_PlaceName));

                System.out.println(" id= " + palceId + " name= " + placeName1);

                ChildItem childItem = new ChildItem(cId, palceId, placeName1, gId);
                childItemList.add(childItem);

                palceRs.close();
            }
            groupItems.add(groupItem);
            groupSearch.add(groupItem);
            childItems.put(groupItems.get(i), childItemList);
            childSearch.put(groupItems.get(i), childItemList);
            rs.close();
        }
    }

    int getPlaceId(int groupPosition, int childPosition) {

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        return item.ChildItemId;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (GroupItem wp : groupSearch) {
                if (wp.GroupItemName.toLowerCase(Locale.getDefault()).contains(charText)) {
                    groupItems.add(wp);
                } else {
                    List<ChildItem> childItemList = childSearch.get(wp);
                    boolean itHave =false;
                    for (ChildItem ci : childItemList) {
                        if (ci.ChildItemName.toLowerCase(Locale.getDefault()).contains(charText)) {
                            itHave=true;
                        }
                    }
                    if(itHave){
                        groupItems.add(wp);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    public void addAll() {
        groupItems.clear();
        groupItems.addAll(groupSearch);
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.list_group, parent, false);
            v.setTag(R.id.lblListHeader, v.findViewById(R.id.lblListHeader));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListHeader);

        GroupItem item = (GroupItem) getGroup(groupPosition);

        lblListHeader.setText(item.GroupItemName);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListItem;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.list_item, parent, false);
            v.setTag(R.id.lblListItem, v.findViewById(R.id.lblListItem));
        }
        lblListItem = (TextView) v.getTag(R.id.lblListItem);

        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        lblListItem.setText(item.ChildItemName);

        return v;
    }


    private class GroupItem {
        final String GroupItemName;
        final int Id;
        int GroupItemId;

        GroupItem(int id, int GroupItemId, String GroupItemName) {
            this.Id = id;
            this.GroupItemId = GroupItemId;
            this.GroupItemName = GroupItemName;
        }
    }

    private class ChildItem {
        final String ChildItemName;
        final int Id;
        public int ChildItemId, GroupItemId;

        ChildItem(int id, int ChildItemId, String ChildItemName, int groupItemId) {
            this.Id = id;
            this.ChildItemId = ChildItemId;
            this.ChildItemName = ChildItemName;
            this.GroupItemId = groupItemId;
        }
    }
}
