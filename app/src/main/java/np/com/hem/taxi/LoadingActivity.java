package np.com.hem.taxi;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.SeekBar;

public class LoadingActivity extends AppCompatActivity {

    private Intent myIntent;
    SeekBar seekBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            seekBar.setProgress(progressStatus);
                        }
                    });
                    if (progressStatus == 100) {
                        myIntent = new Intent(LoadingActivity.this, MainActivity.class);
                        startActivity(myIntent);
                    }
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}

