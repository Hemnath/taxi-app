package np.com.hem.taxi;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "Taxi.db";
    public static final String TABLE_NAME_Districts = "districts";
    public static final String TABLE_NAME_Place = "place";
    public static final String TABLE_NAME_Driver = "driver";
    public static final String COLUMN_ID = "id";

    public static final String COLUMN_DistrictsId = "districtsId";
    public static final String COLUMN_DistrictsName = "districtsName";
    public static final String COLUMN_PalceId = "placeId";
    public static final String COLUMN_PlaceName = "placeName";
    public static final String COLUMN_DriverId = "driverId";
    public static final String COLUMN_DriverName = "driverName";
    public static final String COLUMN_DriverAddress = "driverAddress";
    public static final String COLUMN_DriverPhone = "driverPhone";
    public static final String COLUMN_Taxi_No = "driverTaxiNo";
    public static final String COLUMN_DriverImages = "driverImages";
    public static final String COLUMN_DriverLicence = "driverLicence";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table districts" +
                        "(id integer primary key,districtsId integer, districtsName text)"
        );

        db.execSQL(
                "create table place" +
                        "(id integer primary key,placeId integer, placeName text,districtsId integer)"
        );
        db.execSQL(
                "create table driver" +
                        "(id integer primary key,driverId integer, driverName text, driverAddress text, driverPhone text, driverTaxiNo text, driverImages text, driverLicence text,placeId integer)"
        );

        db.execSQL(
                "create table updateDetails" +
                        "(id integer primary key, lastUpdate text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS districts");
        db.execSQL("DROP TABLE IF EXISTS place");
        db.execSQL("DROP TABLE IF EXISTS driver");
        db.execSQL("DROP TABLE IF EXISTS updateDetails");
        onCreate(db);
    }

    public boolean insertPlace(int placeId, String placeName, int districtsId) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        //onUpgrade(dbRead,0,1);
        int plceId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from place", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            plceId = res.getInt(res.getColumnIndex(COLUMN_PalceId));
            if (plceId == placeId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updatePlace(ID, placeId, placeName, districtsId);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("placeId", placeId);
            contentValues.put("placeName", placeName);
            contentValues.put("districtsId", districtsId);
            db.insert("place", null, contentValues);
            return true;
        }

    }

    public boolean updatePlace(int ID, int placeId, String placeName, int districtsId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("placeId", placeId);
        contentValues.put("placeName", placeName);
        contentValues.put("districtsId", districtsId);
        db.update("place", contentValues, "id = ? ", new String[]{Integer.toString(ID)});
        return true;
    }

    public boolean insertDistricts(int districtsId, String districtsName) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        // onUpgrade(dbRead,0,1);
        int dtsId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from districts", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            dtsId = res.getInt(res.getColumnIndex(COLUMN_DistrictsId));
            if (dtsId == districtsId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updateDistricts(ID, districtsId, districtsName);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("districtsId", districtsId);
            contentValues.put("districtsName", districtsName);
            db.insert("districts", null, contentValues);
            return true;
        }

    }

    public boolean updateDistricts(int ID, int districtsId, String districtsName) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("districtsId", districtsId);
        contentValues.put("districtsName", districtsName);
        db.update("districts", contentValues, "id = ? ", new String[]{Integer.toString(ID)});
        return true;
    }

    public boolean insertDriver(int driverId, String driverName, String driverAddress, String driverPhone, String driverTaxiNo, String driverImages, String driverLicence, int placeId) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        // onUpgrade(dbRead,0,1);
        int drsId, ID = 2;
        Cursor res = dbRead.rawQuery("select * from driver", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            drsId = res.getInt(res.getColumnIndex(COLUMN_DriverId));
            if (drsId == driverId) {
                have = true;
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updateDriver(ID, driverId, driverName, driverAddress, driverPhone, driverTaxiNo, driverImages, driverLicence, placeId);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("driverId", driverId);
            contentValues.put("driverName", driverName);
            contentValues.put("driverAddress", driverAddress);
            contentValues.put("driverPhone", driverPhone);
            contentValues.put("driverTaxiNo", driverTaxiNo);
            contentValues.put("driverImages", driverImages);
            contentValues.put("driverLicence", driverLicence);
            contentValues.put("placeId", placeId);
            db.insert("driver", null, contentValues);
            return true;
        }

    }


    public boolean updateDriver(int ID, int driverId, String driverName, String driverAddress, String driverPhone, String driverTaxiNo, String driverImages, String driverLicence, int placeId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("driverId", driverId);
        contentValues.put("driverName", driverName);
        contentValues.put("driverAddress", driverAddress);
        contentValues.put("driverPhone", driverPhone);
        contentValues.put("driverTaxiNo", driverTaxiNo);
        contentValues.put("driverImages", driverImages);
        contentValues.put("driverLicence", driverLicence);
        contentValues.put("placeId", placeId);
        db.update("driver", contentValues, "id = ? ", new String[]{Integer.toString(ID)});
        return true;
    }

    public boolean insertUpdateDetails(String lastUpdate) {
        // onUpgrade(dbRead,0,1);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("lastUpdate", lastUpdate);
        db.insert("updateDetails", null, contentValues);
        return true;
    }

    public void deleteUpdateDetails() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from updateDetails", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("updateDetails", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public String getLastUpdate() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from updateDetails", null);
        res.moveToFirst();
        String var=res.getString(1);
        res.close();
        return var;
    }

    public int numberOfRows(String tableName) {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, tableName);
        return numRows;
    }

    public Integer deleteDistricts(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("districts",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public void deleteAllDistricts() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from districts", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("districts", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public Integer deletePlace(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("place",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public void deleteAllPlace() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from place", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("place", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public Integer deleteDriver(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("driver",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public void deleteAllDriver() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from driver", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("driver", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public Cursor getDistricts(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from districts where id=" + Integer.toString(id) + "", null);
        return res;
    }

    public Cursor getPlace(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from place where id=" + Integer.toString(id) + "", null);
        return res;
    }

    public Cursor getDriver(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from driver where id=" + Integer.toString(id) + "", null);
        return res;
    }

    public ArrayList<Integer> getAllDistricts() {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from districts", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public ArrayList<Integer> getAllPlace(int id) {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from place where districtsId=" + Integer.toString(id) + "", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public ArrayList<Integer> getAllDriver(int id) {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from driver where placeId=" + Integer.toString(id) + "", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
}