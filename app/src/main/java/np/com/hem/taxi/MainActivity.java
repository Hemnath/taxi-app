package np.com.hem.taxi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    ExpandableViewAdapter listAdapter;
    ExpandableListView expListView;
    int lastExpandedPosition = -1;

    DBHelper dbHelper;
    private Intent myIntent;

    ProgressDialog progressBar;
    private Date lastUpDate;
    private SimpleDateFormat sdf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

      /*  DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);*/

        dbHelper = new DBHelper(this);
        sdf = new SimpleDateFormat("dd/MM/yyyy");

        expListView = (ExpandableListView) findViewById(R.id.lvExp);

        if (isConnected()) {
            DBDataInsert();
        }

        listAdapter = new ExpandableViewAdapter(this);
        expListView.setAdapter(listAdapter);
        listAdapter.setOrderList(this);


        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    expListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                int placeId = listAdapter.getPlaceId(groupPosition, childPosition);
                myIntent = new Intent(getApplicationContext(), DriverDetailsActivity.class);
                myIntent.putExtra("placeId", placeId);
                startActivity(myIntent);
                return false;
            }
        });
    }

    private void DBDataInsert() {

       /* Calendar c = Calendar.getInstance();
        try {
            lastUpDate = sdf.parse(dbHelper.getLastUpdate());
        } catch (ParseException e) {
            e.printStackTrace();
        }*/

        if (isConnected()) {
            new districtsJSONParse().execute();
            progressBar = new ProgressDialog(MainActivity.this);
            progressBar.setMessage("Downloading...");
            progressBar.setCanceledOnTouchOutside(false);
            progressBar.show();
        }

    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Closing Activity")
                    .setMessage("Are you sure you want to close this activity?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            System.exit(0);
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, MainActivity.class)));
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {

            if (isConnected()) {
                DBDataInsert();
            } else {
                showAlertDialog(MainActivity.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        listAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        listAdapter.filter(newText);
        return false;
    }

    private class districtsJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            String url = "http://192.168.0.117:8090/quickcab/district.php";

            JSONParser jParser = new JSONParser();
            org.json.JSONObject json = jParser.getJSONFromUrl(url);
            System.out.println("json = " + json);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            //Date createDate=null,updateDate = null;
            try {

                JSONArray jsonArray = json.optJSONArray("district");
                dbHelper.deleteAllDistricts();
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String id = jsonObject.optString("id").toString();
                    String name = jsonObject.optString("name").toString();
                    String createAt = jsonObject.optString("createAt").toString();
                    String updatedAt = jsonObject.optString("updatedAt").toString();
                    System.out.println("name = " + name);

                    /*try {
                        createDate = sdf.parse(createAt);
                        updateDate= sdf.parse(createAt);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                   if (lastUpDate.after(updateDate)|| lastUpDate.after(createDate) ) {
                        dbHelper.insertDistricts( Integer.parseInt(id), name);
                    }*/
                    dbHelper.insertDistricts(Integer.parseInt(id), name);

                }
                new placeJSONParse().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class placeJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            String url = "http://192.168.0.117:8090/quickcab/place.php";

            JSONParser jParser = new JSONParser();
            org.json.JSONObject json = jParser.getJSONFromUrl(url);
            System.out.println("json = " + json);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {

            try {
                JSONArray jsonArray = json.optJSONArray("place");
                dbHelper.deleteAllPlace();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String id = jsonObject.optString("id").toString();
                    String name = jsonObject.optString("name").toString();
                    String parentID = jsonObject.optString("parentID").toString();
                    String createAt = jsonObject.optString("createAt").toString();
                    String updatedAt = jsonObject.optString("updatedAt").toString();
                    System.out.println("name = " + name);

                    dbHelper.insertPlace(Integer.parseInt(id), name, Integer.parseInt(parentID));


                }

                new driverJSONParse().execute();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class driverJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            String url = "http://192.168.0.117:8090/quickcab/Drivers.php";

            JSONParser jParser = new JSONParser();
            org.json.JSONObject json = jParser.getJSONFromUrl(url);
            System.out.println("json = " + json);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {

            try {
                JSONArray jsonArray = json.optJSONArray("driver");
                dbHelper.deleteAllDriver();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String id = jsonObject.optString("id").toString();
                    String name = jsonObject.optString("name").toString();
                    String Address = jsonObject.optString("Address").toString();
                    String liscenceNo = jsonObject.optString("LiscenceNo").toString();
                    String PhoneNo = jsonObject.optString("PhoneNo").toString();
                    String TaxiNO = jsonObject.optString("TaxiNO").toString();
                    String Image = jsonObject.optString("Image").toString();
                    String placeID = jsonObject.optString("placeID").toString();
                    String createAt = jsonObject.optString("createAt").toString();
                    String updatedAt = jsonObject.optString("updatedAt").toString();
                    System.out.println("name = " + name);

                    dbHelper.insertDriver(Integer.parseInt(id), name, Address, PhoneNo, TaxiNO, Image, liscenceNo, Integer.parseInt(placeID));

                }
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }

                listAdapter.setOrderList(getApplicationContext());
                listAdapter.notifyDataSetChanged();
                /*Calendar c = Calendar.getInstance();
                String fordDate = sdf.format(c.getTime());
                dbHelper.deleteUpdateDetails();
                dbHelper.insertUpdateDetails(fordDate);*/

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}
