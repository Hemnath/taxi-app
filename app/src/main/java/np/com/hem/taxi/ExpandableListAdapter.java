package np.com.hem.taxi;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by manish on 6/6/2016.
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private final DBHelper mydb;
    private Context mContext;
    private HashMap<DriverItem, List<ChildItem>> childItems = new HashMap<DriverItem, List<ChildItem>>();
    private List<DriverItem> groupItems = new ArrayList<DriverItem>();
    private List<DriverItem> groupSearch = new ArrayList<DriverItem>();
    private LayoutInflater inflater;

    public ExpandableListAdapter(Context context) {
        this.mContext = context;
        mydb = new DBHelper(context);
    }

    public void setOrderList(Context context, int placeId) {
        ArrayList<Integer> array_list;
        ArrayList<Integer> array_list_child;
        array_list = mydb.getAllDriver(placeId);
        int count = array_list.size();
        for (int i = 0; i < count; i++) {
            Cursor rs = mydb.getDriver(array_list.get(i));
            rs.moveToFirst();
            int Id = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_ID));
            int driverId = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_DriverId));
            String driverName = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_DriverName));
            String Taxi_No = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_Taxi_No));
            String driverImages = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_DriverImages));
            String driverLicence = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_DriverLicence));
            String driverPhone = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_DriverPhone));
            int strPlaceId = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_PalceId));

            List<ChildItem> childItemList = new ArrayList<ChildItem>();

            ChildItem childItem = new ChildItem(Id, driverId, driverName, Taxi_No, driverImages, driverLicence, driverPhone, strPlaceId);
            DriverItem dItem = new DriverItem(Id, driverId, driverName, Taxi_No, driverImages, driverLicence, driverPhone, strPlaceId);

            groupItems.add(dItem);
            groupSearch.add(dItem);
            childItemList.add(childItem);
            childItems.put(groupItems.get(i), childItemList);
            rs.close();
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (DriverItem wp : groupSearch) {
                if (wp.driverName.toLowerCase(Locale.getDefault()).contains(charText) || wp.driverTaxiNo.toLowerCase(Locale.getDefault()).contains(charText)) {
                    groupItems.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public boolean clearItem() {
        groupItems.clear();
        return true;
    }

    public void addAll() {
        groupItems.clear();
        groupItems.addAll(groupSearch);
    }

    @Override
    public int getGroupCount() {
        return groupItems.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItems.get(groupItems.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.groupItems.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childItems.get(this.groupItems.get(groupPosition))
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView lblListHeader;

        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.driver_list_group, parent, false);
            v.setTag(R.id.lblListDriver, v.findViewById(R.id.lblListDriver));
        }
        lblListHeader = (TextView) v.getTag(R.id.lblListDriver);

        DriverItem item = (DriverItem) getGroup(groupPosition);

        lblListHeader.setText(item.driverName);

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v = convertView;
        TextView phone, taxiNo, licence;
        ImageView driverImage;
        final String strPhoneNo;
        Button callBtn;
        if (v == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = infalInflater.inflate(R.layout.driver_list_details_item, parent, false);
            v.setTag(R.id.phone, v.findViewById(R.id.phone));
            v.setTag(R.id.taxiNo, v.findViewById(R.id.taxiNo));
            v.setTag(R.id.licence, v.findViewById(R.id.licence));
            v.setTag(R.id.driverImage, v.findViewById(R.id.driverImage));
            v.setTag(R.id.callBtn, v.findViewById(R.id.callBtn));
        }
        phone = (TextView) v.getTag(R.id.phone);
        taxiNo = (TextView) v.getTag(R.id.taxiNo);
        licence = (TextView) v.getTag(R.id.licence);
        callBtn = (Button) v.getTag(R.id.callBtn);
        driverImage = (ImageView) v.getTag(R.id.driverImage);


        ChildItem item = (ChildItem) getChild(groupPosition, childPosition);

        phone.setText(item.driverPhone);
        taxiNo.setText(item.driverTaxiNo);
        licence.setText(item.driverLicence);
        strPhoneNo = item.driverPhone;
        if (!item.driverImages.equalsIgnoreCase("") || !item.driverImages.isEmpty()) {
            Picasso.with(mContext).load(item.driverImages)
                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
                    .error(R.drawable.no_image)
                    .into(driverImage);
        }
        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DriverDetailsActivity) mContext).call(strPhoneNo);
            }
        });
        return v;
    }


    private class DriverItem {
        final String driverName, driverTaxiNo, driverImages, driverLicence, driverPhone;
        final int driverId, Id, strPlaceId;

        DriverItem(int id, int driverId, String driverName, String driverTaxiNo, String driverImages, String driverLicence, String driverPhone, int strPlaceId) {
            this.Id = id;
            this.driverId = driverId;
            this.driverName = driverName;
            this.driverTaxiNo = driverTaxiNo;
            this.driverImages = driverImages;
            this.driverLicence = driverLicence;
            this.driverPhone = driverPhone;
            this.strPlaceId = strPlaceId;
        }
    }

    private class ChildItem {
        final String driverName, driverTaxiNo, driverImages, driverLicence, driverPhone;
        final int driverId, Id, strPlaceId;

        ChildItem(int id, int driverId, String driverName, String driverTaxiNo, String driverImages, String driverLicence, String driverPhone, int strPlaceId) {
            this.Id = id;
            this.driverId = driverId;
            this.driverName = driverName;
            this.driverTaxiNo = driverTaxiNo;
            this.driverImages = driverImages;
            this.driverLicence = driverLicence;
            this.driverPhone = driverPhone;
            this.strPlaceId = strPlaceId;
        }
    }

}
